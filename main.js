// Get UI elements
const recordButton = document.getElementById('recordButton');
const recordingsList = document.getElementById('recordingsList');
const modal = document.getElementById('modal');
const recordingName = document.getElementById('recordingName');
const saveButton = document.getElementById('saveButton');
const cancelButton = document.getElementById('cancelButton');

// Initialize MediaRecorder
let mediaRecorder;
let audioChunks = [];

// Event listeners
recordButton.addEventListener('click', toggleRecording);
saveButton.addEventListener('click', saveRecording);
cancelButton.addEventListener('click', cancelSave);

function toggleRecording() {
  if (mediaRecorder && mediaRecorder.state === 'recording') {
    mediaRecorder.stop();
  } else {
    navigator.mediaDevices.getUserMedia({ audio: true })
      .then(stream => {
        mediaRecorder = new MediaRecorder(stream);
        mediaRecorder.start();

        audioChunks = [];
        mediaRecorder.addEventListener('dataavailable', event => {
          audioChunks.push(event.data);
        });

        mediaRecorder.addEventListener('stop', () => {
          const audioBlob = new Blob(audioChunks);
          const audioUrl = URL.createObjectURL(audioBlob);
          const audio = new Audio(audioUrl);
          audio.onloadedmetadata = () => {
            showModal(audio.duration);
          };
        });

        updateUI('recording');
      });
  }
}

function updateUI(state) {
  if (state === 'recording') {
    recordButton.innerHTML = '<i class="fas fa-stop"></i> Stop';
    recordButton.classList.remove('bg-red-500');
    recordButton.classList.add('bg-gray-500');
  } else {
    recordButton.innerHTML = '<i class="fas fa-microphone"></i> Record';
    recordButton.classList.remove('bg-gray-500');
    recordButton.classList.add('bg-red-500');
  }
}

function showModal(duration) {
  modal.classList.remove('hidden');
  recordingName.value = '';
  saveButton.dataset.duration = duration;
}

function saveRecording() {
  const name = recordingName.value;
  const duration = saveButton.dataset.duration;
  addRecordingToList(name, duration);
  modal.classList.add('hidden');
  updateUI('stopped');
}

function cancelSave() {
  modal.classList.add('hidden');
  updateUI('stopped');
}

function addRecordingToList(name, duration) {
  const listItem = document.createElement('div');
  listItem.classList.add('border', 'border-gray-300', 'rounded', 'p-4', 'my-2');
  listItem.innerHTML = `
    <h2 class="font-bold">${name}</h2>
    <p>${Math.round(duration)} seconds</p>
  `;
  recordingsList.appendChild(listItem);
}
